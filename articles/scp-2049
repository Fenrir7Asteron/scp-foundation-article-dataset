Item #: SCP-2049
Object Class: Keter
Special Containment Procedures: Ten Foundation personnel should reside in Manningtree, UK, at all times. When a transmission from SCP-2049-1 is detected, information regarding the forecast should be related to Site 294 immediately. If the resulting anomaly directly affects the population of Manningtree, MTF 294-Samekh ("We Get To Choose Our Own Name?") will be deployed to Manningtree. A cover story is to be established for the results of the resulting anomaly, and amnestics are to be administered to any direct witnesses.
SCP-2049 broadcasts have been reported to Manningtree as pranks originating from an unknown source in the United Kingdom.
Description: SCP-2049 is a periodic anomalous weather forecast that only occurs in Manningtree. At random intervals,1 a broadcast will appear on channel 43.52 Manningtree, usually between 08:00 and 14:00. At this time, SCP-2049-1 appears on screen and delivers a weather forecast, typically predicting anomalous weather conditions. Exactly 24 hours after the broadcast terminates, the predicted weather conditions will occur over or near Manningtree. (See Document 2049-Theta for a full log of forecasts, to date.)
SCP-2049-1 is a humanoid with teal skin. Other than this quality, SCP-2049-1 appears to be a non-anomalous human male in his 40's. SCP-2049-1 delivers the forecasts that appear in Manningtree, although it has stated that the anomalous effect of the forecasts is unintentional. (See Interview Log 2049-A.) The following is the first known broadcast from SCP-2049-1, recorded by a civilian from Manningtree.
SCP-2049-1: Good morning, this is Xchtl'krnss3 with your Monday weather report. The heat wave that seemed to slow everyone down over the weekend has only worsened since the last forecast! Our meteorologists predict that today's temperature could climb to as high as 40 degrees Celsius by the late afternoon. Right now, the temperature seems to be around 29 degrees Celsius, and it's only the morning. However, there is a colored breeze coming in on Tuesday, which might help alleviate the heat. Unfortunately, there is also no rain in the forecast for the next five days. This is bad news for all you gardening enthusiasts out there. If you don't have air-conditioning, you might just be sleeping on the roof tonight! That's all for today's weather. Stay tuned for the Tuesday weather report.
This weather report was deemed as targeted at Earth, which was discovered to occur only as a rare occurrence. Most other weather reports pertain to currently unknown planets and/or realities.
Over time, it is found that these broadcasts have decreased both in signal strength, and A/V quality. Both of these decreased drastically between two broadcasts, just prior to the original inquiry for sponsors, and slowly over time.
Interviewed: SCP-2049-1
Interviewer: Doctor Fitzpatrick
Foreword: Subject is contacted via provided phone number during a broadcast. The number that the interviewer calls from is blocked.
<Begin Log, 13:24:09>
SCP-2049-1: Yes, hello? This is the Center of Interdimensional Weather speaking.
Doctor Fitzpatrick: Is this the line for sponsors?
SCP-2049-1: Yes! Yes, it is! <Doctor Fitzpatrick holds the phone away from his ear. SCP-2049-1 exclaims this loudly.>
Doctor Fitzpatrick: Could you please inform me as to what you do, exactly?
SCP-2049-1: Oh, sure, sure. I'm Xchtl'krnss, but, heh, I guess you know that already! What can I put you down for?
Doctor Fitzpatrick: Are you aware that you are creating the weather that you forecast? Are you able to predict future events?
SCP-2049-1: <Laughs.> Preposterous. I, like any good weatherman, just report the facts as I see 'em.
Doctor Fitzpatrick: Your broadcasts seem to have decreased in quality over time. Why? Did something happen?
SCP-2049-1: Oh. Oh, uh… yeah. Yeah, my employer… well, they, uh, let me go. They said that interdimensional weather really wasn't cutting it anymore. That people preferred the domestic stuff. I think… I think they do some kind of shopping network now.
Doctor Fitzpatrick: How is it that you continue your broadcasts?
SCP-2049-1: Oh, I've been using my own camera. I bought this frequency, right? They gave it to me pretty cheap. It was… uhm, the least they could do, they said.
Doctor Fitzpatrick: Have you gained any sponsors?
SCP-2049-1: Oh, yeah! Quite a few. I'm proud.
Doctor Fitzpatrick: Can you divulge their names?
SCP-2049-1: N-no, because… because… uhm, 'cause that's… yeah, that's confidential, ma'am. <Note that Doctor Fitzpatrick is male.>
Doctor Fitzpatrick: Okay. Why do you continue these broadcasts? Do you have many viewers?
SCP-2049-1: <No response.>
Doctor Fitzpatrick: Hello? Kic- <Interviewer begins to attempt pronunciation of SCP-2049-1's given name, but is interrupted.>
SCP-2049-1: It gets lonely out here, okay? At least I still have those loyal few viewers. They seem to love me. They tune into me, every time, without fault. They trust me.
Doctor Fitzpatrick: Okay, SCP-2049-1.
SCP-2049-1: …who?
Doctor Fitzpatrick: I think that will be all.
<An animal-like whimper can be heard through the telephone.>
SCP-2049-1: Please don't go.
<End Log, 13:31:37>
Closing Statement: A second call to this number failed, returning with a statement that the number did not exist. Broadcasts still occur, and are added to this documentation as they take place. Neither the broadcast quality, nor SCP-2049-1's demeanor, have improved since the call.