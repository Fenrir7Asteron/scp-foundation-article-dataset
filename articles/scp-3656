Current appearance of SCP-3656.
Item #: SCP-3656
Object Class: Neutralised
Special Containment Procedures: The area previously containing SCP-3656 is surrounded by a chain-link fence and monitored by CCTV. Signage is to denote the area as containing hazardous chemical waste to deter civilian intrusion. If it is necessary for staff to enter the area, appropriate protective gear is required and use of personal dosimeters and Geiger counters is advised.
Description: SCP-3656 is a 5.5 km2 field, formerly Site-70 and the greater part of the suburb of ████████, in outer Chicago, Illinois. It is contaminated by high levels of radioactive isotopes, mostly strontium-90 and caesium-137. The average radiation across the area is approximately 50 kBq/m2.
SCP-3656 demonstrates no current anomalous effects.
Central building of Site-70.
Item #: SCP-3656
Object Class: Euclid
Special Containment Procedures: One Foundation staff member (Representative A-1) has been designated the primary point of contact with SCP-3656. On the 4th day of each month at 9am (UTC-6:00), this staff member is to lead a team into SCP-3656 and document the proceedings. All events within SCP-3656 should be audio recorded and transcribed. Overt video and photographic recording is prohibited as its use causes entities within SCP-3656 to react with suspicion. Clandestine recording requires Level 4 staff approval.
All staff entering SCP-3656 should be of Iranian ethnicity, fluent in Farsi and with comprehensive knowledge of Iranian culture and history, particularly of the Safavid dynasty (1501-1736). Representative A-1 should be a Level 3 staff member with experience in public policy, international law and diplomatic negotiation. Staff within SCP-3656 are to identify themselves as representatives of the “Sublime State of Iran in Exile” (Group-A), a group known to SCP-3656 entities prior to Foundation involvement. Appropriate documents and identification have been prepared for this purpose, and no materiel with Foundation information or insignia is to enter SCP-3656. Staff are advised not to consume any items within SCP-3656.
Personnel who enter SCP-3656 require an additional briefing and memetic resistance screening conducted by the SCP-2155 research team. All documents making detailed reference to Group-H should be considered a memetic hazard and quarantined until their anomalous status can be determined.
Description: SCP-3656 is a room in the basement of the house at 50 Adams St., ████████, Chicago, Illinois. The address was formerly the residence of Mr Ali Mirzakhani, a businessman and member of the Iranian community in Chicago. Since its acquisition by the Foundation in 2006, the building was given the designation Site-70, which eventually acquired both neighbouring houses to accommodate personnel and research materiel.
On the 4th day of each month, when an individual enters SCP-3656 and closes the door behind them, an SCP-3656 event will occur, where SCP-3656 will take on the appearance of a conference room with a central table and seating for 40 people. A door also appears on the opposite side of the room to the entrance, through which entities enter and exit. Exploration attempts beyond this have met resistance from SCP-3656 entities. While an event is in progress, establishing outside communication or access has been unsuccessful, and GPS tracking devices lose signal.
Between 20 and 40 humanoid entities will be present in SCP-3656, several with anomalous features. All identify themselves as representatives of groups belonging to the “Displaced Governments Cooperation Organisation”, or DGCO (SCP-3656-1). SCP-3656-1 appears to be a supranational organisation with the aim of promoting diplomatic, cultural and economic cooperation. Its member groups describe themselves as governments which have lost control over most or all of their claimed national territory. Despite these circumstances, they claim to possess considerable financial and military resources, with access to both conventional and anomalous weaponry. The entities within SCP-3656 frequently refer to locations, individuals and events for which no evidence has been found in baseline reality.
Thus far, 8 groups have been identified in SCP-3656.
Group-A (Foundation staff only) - "The Sublime State of Iran in Exile"
Group-B - “The Everlasting Tsardom of Russia"
Group-C - “The Friends of the Doge”
Group-D - “The Second Pacifican Republic”
Group-E - “The Association for the Restoration of Japan”
Group-F - "The Unity"
Group-G - "The Republic of Poland"
Group-H - [DATA EXPUNGED]
While an SCP-3656 event is in progress, Representative A-1 is to participate and gather intelligence without raising suspicion, and promote the exercise of restraint and conservatism in policy decisions. If they are asked to make significant policy decisions or SCP-3656 entities have other queries with serious implications, they are advised to make diversionary statements and ask to revisit the issues at the time of the next event, so a course of action can be decided on in the interim with the input of senior staff. Retrieval of objects from within SCP-3656 is a goal so long as it does not attract the attention of SCP-3656 entities.
Addendum 3656-1: Abridged example of an SCP-3656 event
Event Transcript 3656-299-1 (04/11/13)
Foundation Participants: Dr Dabashi (Representative A-1), Agent Nafisi, Agent Hashemi
Text in [square brackets] was translated into English during event by SCP-3656 translators. Entities with designation -1 refer to ‘representatives’, with higher numbers their associates.
Entry of Foundation team into SCP-3656.
C-1: [Welcome, to the honourable Persian delegates.] Other entities voice similar sentiments.
Dr Dabashi: Greetings to the honourable representatives.
C-1: [Now that all are present, I suggest we begin. This meeting of the Organisation is now in session. I, Carlo Contarini, representative of the Venetian people, have the rotating chair, passed to me by His Russian Excellency. Do any of the assembled wish to raise any issues from the last meeting?]
No responses.
C-1: [We shall continue. The primary item on the agenda today is the proposal for currency standardisation, as part of our 20-year plan for development of a single market. As you know, all members have agreed to this in principle, but a number of concerns have been raised. In particular, the representative from Pacifica has strongly expressed his government’s feelings about the proposed use of the New Standard dollar. Ambassador Wilson, you have the floor.]
D-1: The Republic will veto any proposed (unintelligible) if the currency of the occupiers is used. We are aware New America remains dominant in most of your existences, but there is a higher principle at stake here. All members are bound by a common purpose of justice and (unintelligible), which cannot be sacrificed for economic expediency.
B-1: [The exercise of the unilateral veto would be most irregular and disappointing, given that the single currency proposal had been previously agreed to in Resolution 40.]
D-1: Resolution 40 was a statement of intent, which did not give the proposal’s specifics.
F-1: (buzzing for 25 seconds)
E-1: [I agree with the Unity representative. My Association considers the use of the Standard dollar preferable but negotiable. While the Pacifican position poses some problems, they should be able to be accommodated.]
C-1: [The introduction of an entirely new currency, even if pegged to the New Standard dollar, would cause significant disruption to our operations. We would not be able to agree to this without concessions.]
Dr Dabashi: That seems reasonable.
D-1: Is it necessary for the Venetian people to consult the Board before making decisions? Muttering and disquiet.
B-1: [Ambassador, please.]
H-1: [DATA EXPUNGED]
G-1: [I propose we move forward from the point of view of developing a new unified currency. This has always been considered by the Organisation as a possibility throughout this process, and much of the theoretical work has already been done. Parties are still able to provide their conditions.]
E-1: Haven’t had enough of new currencies? (laughter) [We agree with the proposal].
H-1: [DATA EXPUNGED]
B-1: [Agreed.]
F-1: (buzzing for 2 seconds)
D-1: It’s good to see we can come to an agreement.
Dr Dabashi: (whispering to other team members) What do you think?
Agent Hashemi: (whispering) Why are you asking us? You’re the expert here.
Dr Dabashi: (whispering) I worked on free-trade agreements, not currency unions. (to room) We request, uh, to seek the approval of the Shahanshah, prior to making a final decision.
B-1: [As we are still in the development phase of the process, we request a statement of intent at today’s meeting.]
Dr Dabashi: Very well, we, uh, agree with the Minister, at this point in time.
Event Transcript 3656-299-2 (04/11/13)
Foundation Participants: Dr Dabashi (Representative A-1), Agent Nafisi, Agent Hashemi
C-1: [The assembled representatives have proposed and agreed to consider a new unified currency. In the interests of international solidarity, we will continue along this path. I propose a fifteen-minute recess.] General agreement.
The door opposite the entrance of SCP-3656 opens, and several men and women dressed in formal attire enter with water, tea, coffee and platters of sandwiches. SCP-3656 entities begin to move about the room and make casual conversation. C-1, C-2 and C-3 leave the room.
B-2 places a plate of sandwiches in front of B-1. No response is observed from B-1.
B-2: (to D-1) These are very good. (D-1 appears surprised) My apologies, Excellency. I meant no offence.
D-1: None taken.
G-1: (to Dr Dabashi) Would you like a ham sandwich, Ambassador? It’s all right for you to eat these?
Dr Dabashi: Oh, thank you. Yes, I’m not a practicing Muslim. Dr Dabashi takes a sandwich. G-1 appears confused.
E-1: Pardon me for asking, Ambassador, but I didn’t realise you had undergone a realignment. Was it very difficult for you?
Dr Dabashi: Sorry?
E-1: You haven’t? But why – ah. Best not to eat those, Ambassador. Have the egg salad instead.
Dr Dabashi and E-1 share halves of an egg salad sandwich. Dr Dabashi later reports no abnormalities.
Dr Dabashi: Very nice.
Dr Dabashi and E-1 proceed to have a 10-minute conversation about Japanese and Iranian national cuisines. The content of this conversation is unremarkable, save E-1 lacking comprehension when Dr Dabashi attempts to discuss dishes containing salmon, tuna and other seafoods.
E-1: Next year in Isfahan, yes? (laughs and claps Dr Dabashi on the shoulder)
Agent Hashemi successfully retrieved a sample of the ham sandwich. No abnormalities were found on testing, but when a portion was fed to a laboratory mouse, signs of distress and poor appetite developed over 2 hours. An autopsy revealed widespread intestinal ulceration. The remainder of the sandwich was classified as a hazardous anomalous object and is currently in cold storage at Site-70.
Addendum 3656-2: On 04/07/16, the Foundation team entering SCP-3656 did not reemerge after 30 hours. Staff members Dr Dabashi, Agent Hashemi and Junior Researcher Rahemi have been designated as missing in action. Since this date, SCP-3656 has been inactive, with no deviations from an ordinary basement room. SCP-3656 was subsequently reclassified as Neutralised.
An audio recorder issued to Agent Hashemi was found in SCP-3656 after this event. This is the only example of an item persisting within SCP-3656 after the completion of an event. Large sections of the data are missing, and identification of speakers is speculative, as no context information was available.
Audio Log 3656-330-1 (04/07/16)
C-1: [… significant disruption to our activities.]
F-1: (buzzing for 12 seconds)
D-1 (?): As you know, the accession of the Marshal to office has altered the political situation significantly. The administration can no longer countenance the (unintelligible) of the Organisation if there is no mutual support amongst its members.
G-18: [I must remind you that my government has serious reservations about this course of action. It is not consistent with this organisation's stated goals of peaceful cooperation and promoting international justice.]
D-1 (?): International justice is of no use if its enforcement requires allowing an occupying power to trample the oppressed.
E-1: [Minister, we understand your reservations, but we have already recognised your unique situation and agreed your diplomatic support only will be sufficient.]
Dr Dabashi: When was this agreed?
E-1: [The specifics of this agreement were arranged in the emergency session of the Organisation.]
Dr Dabashi: We were not party to this session, and request a summary of the events.
(background noise and unintelligible whispering for 4 minutes)
E-1: [The Persian non-attendance at the emergency session was already noted. Do the other representatives have an opinion on how to proceed?]
H-1 (?): [DATA EXPUNGED]
B-1 (?): [Agreed. This was most disappointing and unexpected. We have been forced to assume that the lack of communication from the Sublime State was a statement of displeasure.]
Dr Dabashi: I assure you, excellency, no offence was intended.
C-1: [This only raises further questions. While we do not wish to comment on your internal affairs, such a lapse reflects poorly on the functioning of your office.]
E-1: [I do not think we should question the good character of our friend the Ambassador. However, this is perhaps a good time to revisit our concerns from the emergency session. His Imperial Majesty himself has taken an interest in the information security procedures of the Organisation, and I gather the Pacifican Republic and [DATA EXPUNGED] share similar concerns, yes?]
G-1: [Are you suggesting that the assembled representatives are not acting in good faith? I must protest… ]
E-1: [Please, Minister, I am not making any accusations. I simply wish to be able to report to His Imperial Majesty that we are confident in the integrity of all present. I am sure there will be no problems, yes?]
(sound of doors opening, movement of 10-20 persons into room)
Dr Dabashi: What is this?
C-1: [General, this is a diplomatic meeting!]
F-1: (buzzing for 8 seconds)
G-1: [You agreed to this?]
D-1 (?): [As have I, and the Russian and [DATA EXPUNGED] representatives.]
C-1: [The Board will not stand for this offence against… ]
E-1: [It is merely a formality, Mr Contarini. In any case, the Board has already given their approval for these measures.]
Dr Dabashi: I must lodge an official protest. We cannot be expected to conduct meetings under such conditions.
E-1: Ambassador, I'm sure we can straighten this all out. I think you deserve an explanation for this unfortunate problem in private. [Major Fujita, would you?]
E-?: [Yes, General.]
E-1: As I said, a mere formality.
[RECORDING ENDS]
Addendum 3656-3: On 20/1/2017, all material within the current bounds of SCP-3656 disappeared, leaving a layer of exposed topsoil. 7 Foundation staff and an estimated 3,200 civilians were affected by this event, and are presumed deceased. The bulk of archived material relating to SCP-3656 was also lost in this event. Responding agents and civilians displayed symptoms of acute radiation poisoning, leading to the discovery of significant radioactive contamination. Widespread administration of Class-B amnestics was required to conceal this event, in combination with a cover story relating to a chemical truck explosion.
Addendum 3656-4: On 09/06/2017, the following document was received by the Provisional National Government of Vietnam, a California-based Vietnamese organisation in opposition to the current Communist government. Foundation agents secured the document and existing copies, explaining its presence as a prank.
Similar documents have been reported by Foundation sources within the Central Tibetan Administration and the Sahrawi Arab Democratic Republic. Any further examples of these should be intercepted and seized, with amnestic administration as necessary.
The whereabouts of SCP-3656-1 are unknown. Site-71 has been established to research methods of contact and containment as a high priority.
Document 3656-1:
The subversion of a peaceful diplomatic organisation for the purposes of espionage represents a crime against international law, peace and stability of the highest gravity. While the Organisation regrets the loss of civilian life, it is the unanimous opinion of the membership that a firm and decisive response is justified against the Persian state, with lesser measures serving to undermine any consensus towards the fragile order that has thus far been achieved across the multiverse.
The Organisation hopes the Vietnamese people will stand beside them in the eternal struggle for freedom, as we have bonds of history, brotherhood and displacement that cannot be broken. We invite your governmental representatives to our next summit at [REDACTED].
Ubinam sum, ibi patriam vitae.9