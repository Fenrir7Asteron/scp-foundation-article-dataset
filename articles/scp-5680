The building containing SCP-5680.
A rare fully-formed avian specimen produced by SCP-5680.
Item #: SCP-5680
Object Class: Edifice
Containment Class: Euclid
Special Containment Procedures: Area 5680 has been established near SCP-5680 to facilitate containment. The area surrounding SCP-5680 has been demarcated with a barbed wire fence. No civilians are to be granted entry into this perimeter.
On a biweekly basis, a semitrailer truck must be driven by a Foundation operative to the gate of SCP-5680. Then, the operative must wait for SCP-5680-1 to load avians or equivalent into the truck's trailer. Care must be taken to ensure that the truck is position in such a way that SCP-5680-1 instances do not need to leave the factory in order to deliver avian specimens into the truck. Before returning to Area 5680, the operator must visually confirm that SCP-5680-1 are not loading any further objects into the truck. After the truck has returned to Area 5680, the resulting biological material may either be incinerated or transported to Foundation sites for use in containing other SCP objects.
Following Incident 5680-Theta, at least three members of security personnel are required to be on-site at all times.
Description: SCP-5680 are a set of assembly lines and machining equipment contained within an industrial site in the Selva de Irati, Navarra, Spain. Of note is SCP-5680's remote location, making it impractical to operate as a factory through standard methods. Markings covering the building indicate that it was built for the production of avian animals. SCP-5680 involves several anomalous machines; for instance, an acid bath capable of converting any matter to bird organs, and a set of robotic arms capable of seamlessly concatenating these parts into an organic lifeform. The remainder of the components are non-anomalous assembly machines. These machines are capable of being operated by non-anomalous humans and SCP-5680-1.
SCP-5680 is staffed by 25 or more instances of SCP-5680-1. SCP-5680-1 are similar in appearance to non-anomalous humans; however, they do not have eyes, and their faces constantly display an expression of grimace. In addition, vivisections of SCP-5680-1 instances have revealed that they have no internal organs and are composed entirely of an organic paste. When removed from the site containing SCP-5680, SCP-5680-1 instances will rapidly decompose into this paste.
SCP-5680-1 instances can only speak using a selection of pre-determined phrases, rendering them unable to communicate beyond their limited vocabulary. As such, their sentience is debated.
So far, SCP-5680-1 have been observed to only use the following phrases. Unless explicitly stated otherwise, each phrase is spoken in a jovial tone.
When objects are placed into the receiving ends of SCP-5680, they will be processed into living avian specimens. SCP-5680 has demonstrated the ability to produce a variety of bird species; while in Foundation containment, it has been observed to produce game birds, gulls, and birds of prey. Thus far, no pattern has been observed.
However, most birds created by SCP-5680 are incomplete. Many lack internal organs, have too many or too few limbs, or have body parts on wrong parts of their body. It is rare for a specimen to live for more than two hours.
A parking lot has been constructed outside of the building containing SCP-5680, along with roads leading into the parking lot. These roads abruptly terminate 0.5 kilometers from SCP-5680, marked with octagonal red signs reading "ROAD ENDS".
In addition to the room containing SCP-5680, the aforementioned industrial site contains a hallway that encompasses the rest of the site's floor area. This hallway, which is approximately 530 meters in length, contains several functional fax machines and water coolers. The hallway is partially illuminated by a series of fluorescent light fixtures placed every 30 meters.
Every three hours, every fax machine simultaneously produces the following document:
 

I am contacting you regarding how business became time and money. The bird factory is estimated to produce 10,000 units between September 2020 and August 2002. I think we want to make money here; I thought this was obvious?
Don't waste any more of our time, cut the slack, Jack!
- Rodney
 
Note: This document is not translated. Despite SCP-5680 being located in Spain, all documents are in American English.
Each fax machine is equipped with an extended tray of paper1 and expanded ink reserves. It is estimated that each fax machine will be able to print copies of the above document for 3 years at minimum.
At the end of the hallway, an image of a door is painted on the terminating wall. This "door" has a doorknob, but is otherwise a complete fabrication. The door is labeled "Clarence - CEO".
Analysis of the industrial site and its contents estimates that it was all created between August 15th and August 22nd of 2018.
On September 23rd, 2020, an unidentified vehicle drove to the industrial site containing SCP-5680. The object was designed in the facsimile of a tanker truck, despite the fact that it very clearly had undergone several modifications making it more circular in shape. Upon arriving at the loading docks, three quadrupedal entities exited the vehicle, leading several instances of SCP-5680-1 into the factory.
Recording apparati on-site recorded the entities communicating in American English.
AUDIO RECORDING
<Begin Log>
Entity A: Hey, paint me a picture. This is what the man upstairs wants in the loop, right?
Entity C: I thought we tabled this conversation, Clarence.
Entity A: We picked the low-hanging fruit, so why hasn't the boss joined up with us yet?
Entity B: They bird are pushing the envelope out there. Think about the end-user perspective.
Entity A: I know, we actioned that. Shouldn't this be a win-win situation?
Entity C: We've got a USP. The birds do nothing but make hay.
Entity C: Put this on your radar, Clarence. We've done nothing but move forward. If we don't buck the trend, who will?
Entity A: You're right, we've got the bandwidth for this.
<End Log>
The entities then boarded their vehicle and left before Foundation response forces could reach them. Attempts to track the truck or discern its destination were unsuccessful.