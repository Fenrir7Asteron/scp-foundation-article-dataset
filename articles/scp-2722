Item #: SCP-2722
Object Class: Safe
Special Containment Procedures: Excavation crews assigned to SCP-2722 are to be rotated on a one-day-on, three-days-off work cycle. All crews are to receive psychological evaluation screenings for signs of abnormal shifts in mental state. Anti-depressant regimens have proven ineffective in counteracting SCP-2722's mental influence, and so avoidance and recovery are paramount.
Items excavated from beneath SCP-2722 are to be transported to Site-21 for further study, outside the object's range of effect.
Description: SCP-2722 is a graphene stele measuring 260 meters in height, 55 meters in width, and resting at an angle of 37 degrees. The object is engraved with text in an estimated 100,000 different scripts and languages, the majority of which bear no resemblance to known Earthly languages. Of those languages that can be read, the text consists largely of seemingly-random combinations of letters and phonemes, save for the following list of names.
All individuals listed died above the Karman Line. They are listed in chronological order, sorted alphabetically per group. Comparison of Earth languages indicates that all segments of text repeat the same information.
At irregular intervals, additional text will appear on SCP-2722, accompanied by a brief white glow and the shrinking of text size as needed. No pattern has yet been determined, if any, to additions.
Upon crossing the minimum safe distance of 5 kilometers, individuals will hear the following phrase through mental transmission.
I AM THE DEATH OF ALL ESCAPE. NONE SHALL PASS BEYOND.
At 2 kilometers, a second message is relayed.
HEAR NOW THE RECORD OF THE LOST.
Upon reaching a distance of half a kilometer, SCP-2722 will begin a recitation of what is presumed to be its complete textual content. Individuals at this range or closer report significant increases in episodes of depression, suicidal thoughts and generalized existential dread.
SCP-2722 will re-start its recitation when an individual re-enters the half-kilometer zone. As of present, no individual has listened to the entire message.
Addendum: Wreckage around the base of SCP-2722 prompted excavation efforts shortly after discovery. Thus far over 17 tons of material, primarily hull plating, has been excavated. It is believed that the primary structure of the vessel remains buried under SCP-2722.
Of recovered materials, the following items have been considered noteworthy: