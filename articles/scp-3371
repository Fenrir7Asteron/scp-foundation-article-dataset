SCP-3371 in containment.
Item #: SCP-3371
Object Class: Euclid
Special Containment Procedures: SCP-3371 is to be contained within a high-grade intangible humanoid containment chamber (minimal furnishing). Requests pertaining to the use of artistic implements are to be denied.
SCP-3371-1 through SCP-3371-7 are to be contained within a high-value object warehouse, and monitored via CCTV cameras. Following a Reanimation Event, relevant containers are to be examined for damages and repaired appropriately.
Description: SCP-3371 is an incorporeal entity bearing heavy resemblance to English contemporary artist Damien Hirst, barring two small, round scars located on its temples. SCP-3371 is entirely intangible, passing through solid surfaces with no evident resistance.
SCP-3371 was initially captured after reports centralized within the Tate Modern art gallery suggested the presence of a male figure 'passing through walls'. MTF Mu-13 ("Ghostbusters") was dispatched, and apprehended SCP-3371 as it observed a series of paintings.
Efforts to locate Hirst for interview have been unsuccessful.
SCP-3371-1 through SCP-3371-7 designate a series of periodically animate exhibitions and objects, all of which SCP-3371 claims to have created during its corporeal state.
'Reanimation Events' tend to occur with a frequency of one every twelve days, each event lasting for approximately four hours. The specific effects are unique to each object, but generally characterised by the movement of a previously deceased animal or inanimate object.


Interviewed: SCP-3371
Interviewer: Dr. Ayoade
Foreword: SCP-3371 had thus far spent its containment pacing the far wall, humming various unidentified songs, and attempting to scratch into its wooden flooring.
[BEGIN LOG]
Dr. Ayoade: Good afternoon.
SCP-3371: (Bitterly) Is it? I couldn't tell, what with the lack of any natural light. I'm going to look awfully pale by the time I get out.
Dr. Ayoade pauses to take notes. SCP-3371 cranes its neck, attempting to read what he has written down.
Dr. Ayoade: We'll see if we can do anything about that.
SCP-3371: Hey, while you're at it- could I have a sketchbook and some pencils? Hell, a biro would be alright, actually. Just something to ease the boredom.
Dr. Ayoade: My superiors have expressed concern over further creations gaining anomalous properties- so for now, I'm afraid the answer is no.
SCP-3371 grumbles discontentedly.
Dr. Ayoade: May I ask a few questions? Nothing particularly personal, just regarding your current state.
SCP-3371: Blimey, if you must. I thought this was a prison.
Dr. Ayoade: Understandably. To begin, what led you to the Tate Modern art gallery? What were your intentions?
SCP-3371: If you're asking me about any big ulterior motives, you're out of luck. It just brought back some memories.
Dr. Ayoade: Go on.
SCP-3371 pauses, grinding its teeth as it thinks.
SCP-3371: For The Love of God- from what I've been told, you're familiar with it- was put on display there way back in twenty-twelve. There's other places, of course, but it just stuck out to me as somewhere I needed to go. (A pause.)
Dr. Ayoade: SCP-3371-2? What about that piece in particular did you find so attractive?
SCP-3371: It's about art, and it's about money, you know? 'Greed will be the death of us'. That's something I've wanted to chase all my life, and that's the closest I've ever gotten. Well, until now, that is.
SCP-3371 folds its arms, smiling toward Dr. Ayoade.
Dr. Ayoade: I'm sorry- what do you mean by that?
SCP-3371: You know, it's- what's the ultimate connection between art and money? Gogh never got his slice of the pie, Kafka kicked the fucking bucket before he was a household name. It sickens me, but it fascinates me. Because you know what death does better than anything else? It makes people listen. It loosens up their wallets. I wanted that. I wanted to make people feel that. So I just sort of-
SCP-3371 raises two fingers to its left temple, creating a mock 'gunshot' sound with its mouth.
- kch kch, boom. Popped my clogs. It's perfect. It's- it's the crux of everything I want to represent. It's my 'magnum opus'.
Dr. Ayoade stares silently for a few seconds. A long sigh can be heard before he speaks again.
Dr. Ayoade: I think we'll stop there. Any special requests before we finish up?
SCP-3371: Get my skeleton up on display somewhere. Gilded.
[END LOG]