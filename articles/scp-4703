


SCP-4703 by PeppersGhost
More by this author

SCP-4703 interior.
Special Containment Procedures: Due to SCP-4703's status as a perfectly legal establishment,1 any direct attempts to shut it down or otherwise prevent members of the public from patronizing the business are considered unlawful. Instead, a Foundation-run grocery store2 has been established adjacent to SCP-4703 to draw away potential customers. Advertising campaigns showcasing the Foundation's superior products and offers are to be sustained in perpetuity.
Description: SCP-4703 is an independently owned supermarket in Midland, Texas, operating under the name 'Yeah, We're Totally Going to Sell You This'.3 It has maintained steady patronage and a legal right to operate despite its engagement in deadly and unethical business practices, which are perfectly legal, thank you very much.
SCP-4703 is distinct from anomalies which directly affect subjects at a baseline memetic or cognitohazardous level, and instead affects underlying bureaucratic systems and semiotic constructs independently from personal cognition (see Addendum 4703-01). Significant cognitive dissonance may still occur due to inherent incongruities between SCP-4703 and normative human comprehension. In addition to sustaining the legality of SCP-4703, these anomalous influences may also form new legal restrictions against certain activities within the property. Violation of these restrictions may result in the spontaneous manifestation of violent animals in or around the offending party.
Examples of unethical (but perfectly legal4) hazards:
The owner of SCP-4703 has not been identified. No correspondence of tactical or scientific value has been made with the owner of SCP-4703.

Addendum 4703-1: Memo regarding ongoing bureaucratic obstacles per Foundation senior legal counsel.
The legal department considers our countervailing of SCP-4703 to be a top-level priority and we are making every effort to resolve the matter in a way that minimizes loss of life and economic detriment. We have received a significant number of inquiries regarding the mechanism of SCP-4703's indisputable legality; unfortunately, there are no easy answers.
Law is a human concept. It exists on paper because we write it down. It exists in practice because we enforce it. Generally we interpret and exercise the law through the scrutiny of semantics, intent, and precedent, yet bureaucratohazards such as SCP-4703 are not necessarily predicated on such things—in fact, the law as most know it has very little to do with the matter. While it's not a perfect comparison, one could say that baseline law is to anomalous law as arithmetic is to algebra: both are recognized as mathematics, but the latter is more abstract.
Imagine that Timmy and Sally each have two apples. If Timmy gives Sally his apples, then Sally should end up with four. But she doesn't. She has ten. How can this be? Sally recounts the apples and reenacts the scenario over and over, but there is no mistake. Two and two make ten. It is an incontrovertible fact. You see, even if anomalies are irrational, they are factual, and it is essential that one accept this if they wish to develop a countervailing methodology.
Once Sally accepts that her four apples have become ten, she reevaluates her radix and decides to recount the apples in base four. Suddenly, the "ten" apples are "10" apples. "10" is four in base four, which is the appropriate number of apples. Eureka! Sally collects another four apples, bringing the total to "twenty", which is "20", which is eight, which confirms that her new paradigm aligns with the abnormality. Form follows the function according to the function of the form, and at last, everything makes sense.
Except none of it does, really. A well-behaved reality oughtn't conflate the concrete with the abstract. If you initially perceived a countable sum of ten apples in base ten, then the equivalent number of apples in base four should be twenty-two, since it stands to reason that changing your perception of the outcome oughtn't alter the physical materials represented in the equation. However, we live in a very naughty reality, and it may, on a whim, allow a young girl to wield apples unbeholden to thermodynamics.
This explanation is inadequate, of course, but hopefully it goes a small way toward helping you understand why the legal department is currently occupied with a comprehensive redrafting of Texas corporate law in a quaternary semiological system. This in itself would be an exceptional feat even for the most skilled of bureaucromancers, and it is further compounded by the necessary incorporation of contingency clauses against the self-aware fact patterns that keep legitimizing rabid lions into existence inside my damned bathroom.
We are grateful to you, our valued colleagues, for your patience and cooperation as we work together toward a solution.
SHELDON M. KATZ, ESQ.
FOUNDATION LEGAL DEPARTMENT

Mission Log: The following is a transcript excerpt from an early field investigation of SCP-4703.
Field Agent: Felicity Blandina (code name 'Karen')
Subject: Daniel Paulson (SCP-4703 Employee)
Foreword: Agent Blandina entered SCP-4703 under the pretense of being a dissatisfied customer.
[Begin Log]
Blandina: I can't believe this. It's a crime. You're scamming innocent people. I'm a mother! I'm here for my kids! What do you want me to tell my kids?
Paulson: Ma'am, I'm sorry about your kids, but that's just how the offer works. It's perfectly legal10.
Blandina: No, I don't accept that. You can't do this. Let me speak to your manager. I want my discount.
Paulson: I don't think the manager will be able to help, ma'am. Look, it says right here on your coupon…
Blandina: You expect me to read your fine print bullcrap? I have astigmatism.
Paulson: …right here on your coupon, it says, "offer valid with ritual castration". Now, I can understand that may seem like an issue if you don't have anything to castrate, but for no extra charge, we can schedule a surgery to attach—
Blandina: I'm not leaving here without my discount.
Paulson: Ma'am, please understand, it can take several months to find a compatible donor.
Blandina: Your lanyard's backwards. Turn it around. I want to see your name.
Paulson: My name's Daniel, ma'am.
Blandina: Listen very closely. I want my discount. I'm going to get my discount. If anyone's getting castrated today, it will be you, and all I would need are my purse strings and my bare hands. Do I make myself clear, Daniel?
Paulson: Now hang on, ma'am. No point in beating the saddle instead of the donkey. I'll talk to the manager and see if we can work something out.
Blandina: I'm going to speak to him myself. Take me to him.
Paulson bites his lip, looks around, and pats his thighs.
Paulson: Right. Okay. This way.
Paulson takes Agent Blandina to a door near the front of the store. It leads to an unlit stairwell which takes them to an underground break room. Several hammocks are suspended throughout the room, each occupied by one or more employees. Paulson crosses beneath a hammock on his stomach and gestures for Agent Blandina to follow him.
Blandina: People sleep here?
Paulson: Some of us live here.
Blandina: Why?
Paulson: Depends. I entered a raffle for an abs transplant and won this instead. Couldn't turn it down. Like, legally couldn't. Some people get lost in the store, and if you don't leave for fifteen minutes after closing, you're legally required to stay. I think a year's the minimum, but the benefits are good, so yeah.
At the other end of the room, Paulson leads Agent Blandina to a dim hall. They pass twelve unmarked doors before reaching the end.
Paulson: He's in here, ma'am.
Paulson opens the door at the end of the hall. The room inside resembles a storage unit with metal shelves along the walls. Several large boxes block the far end of the space.
Blandina: Your manager's office is a supply closet?
Paulson: Basically.
Paulson activates a pull-string light and moves several boxes to reveal an executive office chair in the back of the room. It is turned to face the wall.
Paulson: I really don't think you want to do this.
Blandina: What I want is my discount.
Paulson sighs and draws the chair away from the wall. A nude male corpse, presumably mummified, sits in the seat. Its skin appears desiccated and taut, conforming to the shape of its bones. The figure is postured with its arms draped loosely around its stomach and its head bent backward at a severe angle. The jaw is fully extended, but no teeth are visible. Eyelids are drawn and vacant.
Paulson: This is Mr. Venatio Haruspice, our manager. Sorry. I would have told you, but it's against the rules.
Blandina: I feel like I should have expected this.
Paulson pats his thighs rhythmically for a few seconds.
Paulson: You can still, uh, make your complaint, and he can pass it on to the owner. I don't— I'm not sure how, but he can pass it on, and they'll fax us a response.
Blandina: He can hear us?
Paulson: Maybe. He also might be like a telephone, kind of? I'm not sure. I really don't know. It's, it's very— I'm really sorry for the, uh, the inconvenience. You see, the faxes that the owner sends, they hide them in cereal boxes, so it may take a while to find them, but cereal's all we eat around here, because we get a lot of faxes that are really important, so it hopefully won't take too long, and please rest assured we're trying—
Blandina: David.
Paulson: Me?
Blandina: Explain this, David.
Paulson: Which part?
Blandina gestures broadly to the corpse.
Paulson: I don't know.
Blandina: Tell me what little you do know.
Paulson: I know that he's legally our manager. I know that he's, well, what he is. I know that one of us always has to kiss him goodnight at closing time. I know that if we tell him something, the owner knows, but the owner seems to know everything that happens here anyway, so I can't be certain that's related. What else…? I know that he's empty— or hollow, actually. Hollow's probably a better word.
Blandina: And what's the difference between empty and hollow?
Paulson retrieves his phone from his pocket. He activates the flashlight and illuminates the inside of the corpse's mouth. No teeth, gums, or internal tissue of any sort are present within.
Paulson: See? Just skin.
Paulson moves the flashlight behind the corpse's neck, creating an interior glow at the back of the throat.
Blandina: That's certainly hollow. But how is it not empty?
Paulson: Put your ear up to his mouth.
Blandina: What?
Paulson: Go on.
Blandina: I'm not doing that, David.
Paulson: Oh. Well, it's like a seashell, except you don't hear the ocean. There's a noise. I can't quite tell what it is, though. It's distant. Real far away. Sounds a lot like a voice, but not like speech. Can't even tell if it's human or animal. Roaring, maybe?
Agent Blandina removes the microphone hidden in her blouse and uses the cord to lower it into the corpse's mouth. Speech and noises outside the corpse are muffled.
Paulson: What is that? Are you [Inaudible]
The faint sound of an emergency siren begins to play.
Paulson: Oh, shit. Oh, fuck. It's the lions. Run.
Blandina: Upstairs? But [Inaudible] when we're down here?
Paulson rushes out of the room without responding. Agent Blandina retrieves the microphone and follows after him. Loud, blunt noises are heard behind the hallway doors as she leaves.
[End Log]
Afterword: Agent Blandina exited SCP-4703 with minimal injury. Legality was restructured shortly thereafter to prevent further access to SCP-4703's employee areas by non-employees. An audio analysis of the recording inside Mr. Haruspice's corpse revealed noises which fit Paulson's description. The examiner also noted that the sounds are indistinguishable from human laughter when the recording is played with a 75% increase in speed, which is perfectly legal, thank you very much.