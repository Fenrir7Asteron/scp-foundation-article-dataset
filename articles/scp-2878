Object Class: Euclid
Special Containment Procedures: SCP-2878 is to be contained within an airlocked Class 3 Destructive Entity containment cell. D-class personnel may enter the enclosure only as part of approved testing procedures. Non-expendable personnel are prohibited from entering the enclosure under any circumstances. All cleaning and maintenance is to be performed via non-autonomous drones.
UPDATE 27/07/20██: Any facility that houses SCP-2878 must have at least one Electromechanical Response Team on standby at all times. In the event that SCP-2878 breaches containment and immediate recontainment becomes unfeasible, it is to be summarily terminated by means of standard anti-mechanical ordnance.
Description: SCP-2878 is a vaguely humanoid entity primarily constructed from 316 surgical steel and consumer-grade electronics. Entity stands at 3.2m when fully erect, but normally assumes a hunched posture. It resembles a distorted human in shape, with thick, elongated arms and torso and short, thick legs. While capable of bipedal locomotion, its primary mode of transport is “knucklewalking” using all four limbs. Forelimbs possess sharp, five-pronged gripping appendages retracted during locomotion. Exterior covering originally consisted of lacquered sheet steel but is presently 80% damaged or missing.
SCP-2878 exhibits human or above-human intelligence but distinctly non-human psychology. It possesses great proficiency in the areas of generalized logical reasoning and problem-solving as well as specialized knowledge of mechanical engineering and cybernetic augmentation. It appears to lack any form of social intelligence or awareness and1 does not engage in communicative or recreational activity.
SCP-2878 is predatory and extremely hostile to humans, attacking on sight. Victims are pinned down and vivisected, having a number of their organs removed and prepared for transplantation. Discarded tissues are then externally processed into a semi-liquid slurry and consumed.
Human organs acquired in this manner are used to modify SCP-2878, replacing extant elements of its design. It displays a preference for repairing damaged or missing components but has occasionally been known to replace functional elements. As of 19/02/20██, the following biological modifications are visible:
Addendum 1, Incident SCP-2878-B: Following a safety violation on 21/07/20██, Junior Researcher Bhatia was caught and killed by SCP-2878. SCP-2878 then proceeded to integrate Dr. Bhatia's respiratory system, vocal chords and central nervous system2. Six hours following the death of Dr. Bhatia, SCP-2878 began making wheezing and screaming vocalizations. After 12 hours, the vocalizations had developed into monosyllabic grunts. After 48 hours, on-site researchers recognized vocalizations as garbled Spanish. Project supervisor Dr. G███████ was alerted and 50 hours after the incident, communication was attempted via drone-mounted radio. Transcript follows:
Note: Translated from Spanish except as noted.
<Begin Log, 15:07 23/07/20██>
[SCP-2878 remains completely motionless throughout the conversation. Its speech is garbled and inconsistent, varying wildly in pitch and intonation.]
SCP-2878: Remove, [unintelligible]
Dr. G███████: Hello, SCP-2878. Can you hear me?
[SCP-2878 does not display any physical signs of having noticed the drone.]
SCP-2878: Remain, lacerate, [unintelligible]. Dr. G███████.
Dr. G███████: Excuse me, did you say my name?
SCP-2878 [in English]: Help.
Dr. G███████ [in English]: Help with what?
SCP-2878 [in Spanish again]: Complete. Friend. Metal. Disgust.
Dr. G███████: Can you give some indication that you understand me?
SCP-2878: Dr. G███████. Dr. G███████. ███-███-████-███3.
SCP-2878 [in English, screaming]: Help!
<End Log, 15:09 23/07/20██>
Dr. G███████ immediately ceased communications and notified Site Director K████.
Addendum 2, Incident SCP-2878-B official response:
Dr. G███████, in light of recent revelations regarding the nature of SCP-2878 and its potential as a security risk, the additional containment procedures you requested have been approved.
Appraisal of your other requests is suspended pending psychological evaluation. You have an appointment with Dr. Abalan tomorrow afternoon; SCP-2878 is scheduled for testing next Monday.
- █████ ████, Assistant Site Director