N/A
▸ More by this Author ◂

 
F.A.Q.


SCP-3301.
Special Containment Procedures: SCP-3301 is to be stored within a standard small containment locker at Site-19 when not being used for testing. Testing is only to be authorized by the SCP-3301 head researcher, currently Dr. Benjamin Cole. Testing may only be carried out at the Site-19 E Wing Storage Warehouse, which has been renovated specifically for this purpose.
It is a requirement that all staff members assigned to SCP-3301 must be versed in the rules and protocols of the game. As such, a detailed explanation is available in Addendum 3301.2.
SCP-3301 is a Class IX information security hazard, and its full repercussions are being studied by the Foundation Information Security Department.
Updated Containment Procedures: Per the special memorandum detailed in Addendum 3301.4, testing of SCP-3301 has been made available to all qualified personnel, and is available as an approved extracurricular activity in the Foundation Employee Benefit Program.
Personnel are required to submit detailed testing logs for each 3301 Activation Period. Full log is available here.
Description: SCP-3301 is an ornate, silver box with a latch and silver key. Inscribed on the lip of the box is the following:
THE FOUNDATION
A GAME BY CRYOGEN STUDIOS
A DIVISION OF DR. WONDERTAINMENT
Contained within the box is a board game with the aforementioned title, as well as game pieces and other miscellaneous game parts. The game comes with a small instruction manual detailing a fairly simple resource management board game. However, within the box is a small gold key in a velvet lined drawer, which fits within the lock on the front of the silver box. Inserting the key and turning it while the game box is closed will cause a sliding hatch to open on the top of the box, revealing a flat, purple button inscribed with a white letter “W”.
Pressing this button, labeled within the manual as the “WonderButton”, activates the primary anomalous features of SCP-3301, and begins a game of “THE FOUNDATION”. The following information pertains to the anomalous version of the game.
The baseline properties of SCP-3301 are as follows:
For full description of setup, rules, and gameplay, see Addendum 3301.2.
Addendum 3301.1: Discovery
SCP-3301 was discovered on the desk of Director Tilda Moose at Site-19 on July 3rd, 2017. Attached to the exterior of SCP-3301 was a letter in a silver envelope bearing the word "Foundation". The contained letter reads as follows:
Dear SCP Foundation,
Sentient creatures so often seek purpose within their own lives, whether it be mundane or extraordinary. There are some who would dedicate their lives to healing the sick, or feeding the hungry. Others would try to resurrect their broken gods, or sail through the stars on the arms of a cosmic starfish. Some would rid the world of the unnatural, or foster it in the halls of their Library, and some are just in it for the memes†.
You seek to protect the world from the anomalous, categorize and classify the strange and unique, and let humanity bask in the light. We seek to make people smile, and give people a reason to be happy, if only for a short time. Despite our differences, we cannot help but respect your motives. You've no doubt saved us a dozen times over, so we want to return the favor. We want to make you smile!
Contained within this box is our greatest toy yet. There are no jokes here, no gaffs or plotting. Everything we have at our disposal, every scrap of information and wisp of arcana is contained within this game. We made it because, at the end of the day, we feel like you have the most interesting story to tell. We sincerely hope you enjoy it!
Yours most splendidly,
Dr. Wondertainment
P.S. We are very excited about this product, and believe it is nearly ready for production! But it wouldn't be acceptable to ship a product without playtesting it. So this version we're sending to you, our sole playtester, in the hopes that you'll be able to give us feedback on how we can make our game better! If at any point you stop playing for more than a few days, we'll know that you're done testing it and we'll go ahead and put it on the shelves! If you don't think it's ready for the shelves, then just keep on testing it ;)
P.P.S. Please submit all playtesting suggestions to:
111 Wondertainment Way
Wondertainmentville, Wondertainmentland 10101
† Yes, children, we see you there. We hope you're having fun too.
Addendum 3301.2: Gameplay
The following is information pertaining to the proper gameplay of SCP-3301. In order to maintain containment of the artifact, monthly testing of SCP-3301 is required. Personnel assigned to SCP-3301 are required to maintain a thorough knowledge of the rules and regulations of the game.
Introduction
The following is an introduction copied directly out of “THE FOUNDATION” gameplay handbook. The handbook is a leather-bound pamphlet with embossed silver print displaying the name of the game, the production studio, and the words “50TH ANNIVERSARY EDITION”.
THE FOUNDATION
A GAME BY CRYOGEN STUDIOS
A DIVISION OF DR. WONDERTAINMENT
Welcome, weary researcher, to THE FOUNDATION! A board game only for the strong willed and mighty of heart. But beware! For danger lurks around every corner, and foul things are creeping in the night. Do you have what it takes to stand betwixt humanity and the darkness? Or will you too be lost to the chaos eternal? Only time will tell! Become Mr. Collector, or maybe Mr. Containment! The choice is yours!
Setup
SCP-3301 is played between 2 to 8 teams of two players. Each primary player chooses a secondary player, to act as their representative on the game board. The primary players are all selected when each set of players places their game pieces1 on the board.
The board contained within SCP-3301 varies depending on which version of the “game” is being played, and is random. Gameplay is similar between each board, though the setting is different (i.e. The Land of the Unclean, Echoes of the Mariana, Bigfoot’s Jungle, The Cosmic Starfish, etc.), and different hostile entities appear as opponents on different boards. Testing has revealed at least twenty-three different game boards, though there are possibly more.
Once the game board is unfolded, and the silver card box placed in the middle of the board in the appropriate space, the surrounding area (a space roughly 300m in diameter) will undergo dramatic anomalous spatial changes. Observers will see the space appear to fall away, as if the viewpoint of observers outside of the area of effect has become a bird’s eye view of a space much larger than the affected area. This observed space will mimic the current game board, only on a much larger scale and typically built into an arena setting.
Within the affected space, the primary players will find themselves seated around a flat, crystalline surface suspended above the aforementioned game board arena, while their game piece representatives will be at the starting positions on the game board below. The start of the game is typically accompanied by music and fireworks, and a voice announcing the beginning of a new game. Once the game has begun, nothing can be passed through the spatial distortion surrounding the game arena. Secondary players who are killed or die in-game will appear outside of the distortion shortly after their death, unharmed. Primary players will remain within the anomaly for the duration of the game.
Gameplay
After the introduction has finished, the game begins. Each player starts the game by drawing seven cards from the silver box in the middle of the board, and end each turn by drawing one. If a player has more than ten cards in their hand at the end of a turn, they must discard one by returning it to the silver box. There are several types of cards that can appear in the box, and each affects the game in different ways:
For examples of game cards used in testing, see Addendum 3301.3.
On the outside of the game board is a slider pointing to different difficulty settings. This slider can be moved before the game has begun, but retracts into the board afterwards. The difficulty settings are as follows:
The game is played in a team vs. team format, where each two person team competes against every other two person team. Every player begins at their “containment facility”, a starting point and base of operations on their side of the board. Within the facility, each player has several spaces for “equipment”, which they can swap out at the beginning of their turn with cards from their hand, in order to equip different items to their “piece”. Typically, there are two “weapon” spaces, three “equipment” spaces, two “ability” spaces, and one “companion” space. Playing any card into that space “equips” their piece with the item, ability, or companion. If an item, ability, or companion is lost, the physical card will disappear as if made of smoke. Additionally, some cards will increase the overall number of equipment slots (notably the card “Big Bag o’ Taters” will increase the number of weapon and equipment slots from 2 / 3 to 4 / 5).
At the beginning of each turn, the primary player will select one card, and make equipment additions or subtractions if necessary.6 Afterwards, the player will roll two six-sided dice. The number that is rolled corresponds with a radius within which the secondary player may move, or take an action. If the secondary player has a ranged ability or weapon, they may move to the edge of their movement radius and use it, so long as the target is within range.
From this point on, the game proceeds in any number of variations. Based on the cards drawn by the primary players, and the actions taken by the secondary players, any number of outcomes are possible, depending on which victory condition the players choose to seek.
Victory Conditions
There are several victory conditions that players may choose to seek during the course of the game. The following describes conditions needed to reach each end game:
Additionally, the game manual claims that there are many other secret victory conditions to go alongside those listed above. The existence of these secret conditions means that some games may end suddenly and without warning, when a player meets an unexpected condition.9
Rules
The structure of THE FOUNDATION allows for a variety of gameplay styles and approaches to the different goals of the game. In order to seemingly aid this effort, there are very few enumerated rules to the game. The following are the rules of the game, exactly as described in the manual:
The back of SCP-3301 cards.
Addendum 3301.3: Game Cards
The following are examples of cards used within SCP-3301. While not all cards appear to carry relevant information, many appear to be references to anomalies within Foundation containment, or Foundation personnel. As previously mentioned in this document, the full scale of this breach of information security is part of an ongoing investigation.
Several statistics are utilized within the card system, each corresponding to a certain attribute that players have. The statistics are listed below:
The game manual mentions other non-enumerated statistics that players can add to or subtract from, such as Smooth Talking, Memetic Resistance, Ethics, etc. While there are cards that may affect these stats, the only known way to gauge their level is by asking an entity with limited or full omniscience, such as A Librarian.
A Green Card.
Card Title: Darkness Between Dimensions
Subtitle: A Red Reality
Type: Land
Description: Spawns a machine that, after a random amount of time, will transport anything nearby into the Darkness Between Dimensions. Players in the Darkness Between Dimensions can be saved by divine grace, or with the item “Scranton’s Grappling Hook”. After a random amount of time, players will be returned to the board. They will come back… squishier.
Card Title: Babel’s Spire
Subtitle: The Friendly Union of Man and Beast
Type: Structure
Description: Spawns the Babel Spire on a square of your choosing within line of sight. Nearby animals join your side, and gain +2 attack. Those who do not are sacrificed to Babel. Aya aya aya blood blood blood.
Card Title: Island Turtle
Subtitle: Lazy Archipelago
Type: Land
Description: If you are at sea, this card spawns an Island Turtle to ferry you around. If you’re not, the turtle spawns anyway, and dies from dehydration.
Card Title: City of the Gods
Subtitle: Coming Soon To Your Home Town!
Type: Structure
Description: Spawns a haunted city on a square of your choosing within line of sight. Opponents entering City of the Gods have a high chance of being attacked by a +15 ATK / +12 DEF angry deity.
Card Title: A Funny Little Statue
Subtitle: ctrl+c ctrl+v
Type: Trap
Description: If indoors, creates an infinite maze that opponents can only exit with assistance from outside forces. If outdoors, the effect isn’t noticeable, but trust us when we say it’s really bad.
Card Title: A Swedish Furniture Store
Subtitle: Perfectly Normal
Type: Trap
Description: Spawns a friendly and welcoming department store. Players unfortunate enough to be inside the store when it opens will have to fight their way out, if they can get out at all. Bring your own instructions.
A Blue Card.
Card Title: Director Tilda Moose
Subtitle: Not sure how they got here.
Type: Companion
Description: Summons SCP Site-19 Director Moose to act as your companion. Is a literal moose. When riding Director Moose, gain +7 DEF against memes and cognitive hazards.
Stats: 5 ATK / 8 DEF / 9 HP
Card Title: A Sea Slug
Subtitle: A Proper Gentleman
Type: Companion
Description: Spawns A Sea Slug to act as your companion. Wields an anti-materiel rifle. Talks a lot. Can summon a ghostly butler to do your bidding (with limitations!), but has a 10 round cool-down.
Stats: 13 ATK / 2 DEF / 4 HP
Card Title: A Librarian
Subtitle: A Wanderer
Type: Companion
Description: Summons a Librarian to act as your companion. Has innate knowledge about a vast array of creatures and realms. Might be able to read your opponent’s hands, who knows. If attacked, they’ll flee back to their Library. Typical.
Stats: 3 ATK / 3 DEF / 6 HP
Card Title: A Very Loud Bird
Subtitle: oh god make it stop
Type: Companion
Description: Summons some sort of awful eldritch abomination stuffed into the body of a small bird to act as your companion. Despicably loud. Can stun foes and entities, and commune with fellow anomalies. Unless otherwise protected, players who spend too much time in the presence of A Very Loud Bird will slowly lose their minds.
Stats: 8 ATK / 4 DEF / 4 HP
Card Title: Surf Rock Band
Subtitle: Cruising The Starry Skies
Type: Companion
Description: Summons a ghostly Surf Rock Band to act as your companion. Provides your journey with cool evening tunes and casts a calming influence on everything you encounter. May also be able to commune with the Starfish. May also just smoke a lot of weed.
Stats: 5 ATK / 5 DEF / 6 HP
A Red Card.
Card Title: Spear of the Nonbeliever
Subtitle: What’s a King to a God?
Type: Weapon, Ranged
Description: A massive harpoon gun designed to make mortals of gods. Can only be used on Cosmic, Divine, or Supreme Divine beings.
Stats: +20 ATK vs Gods / -5 SPD / -1 DEF
Card Title: Buster Sword
Subtitle: A BFS
Type: Weapon, Melee
Description: A big sword. Requires two hands to hold. Can cleft a man in twain with a single swing. Reduces stealth. Increases upper arm gains.
Stats: +8 ATK / -3 SPD
Card Title: K-Mart Katana
Subtitle: Only The Finest Chinesium
Type: Weapon, Melee
Description: A cheap sword. Low damage, low attack speed. The first choice of suburban ninjas everywhere. Minus points to attack if wearing a fedora.
Stats: -5 ATK / -5 SPD / Fedora Bonus -1 ATK
Card Title: Temporal Tinkering
Subtitle: For When You Need A Few More Seconds
Type: Ability, Temporal
Description: Allows the user to wield the great and terrible cosmic power of time, in very small increments. Users may return up to 10 seconds into the past. Expires after three uses. Paradoxes not included.
Card Title: A Gun That Shoots People
Subtitle: “Shoots” “People”
Type: Weapon, Ranged
Description: A remarkably powerful .50BMG. Definitely not something you want to use yourself. Give it as a gift to an enemy, or anyone else you’d like to see spend the last few seconds of their life as a tiny, remarkably dense, screaming projectile. Expires after use.
Stats: +19 ATK / +5 ACC
Card Title: Intro to Memetics
Subtitle: The Beginner’s Course
Type: Ability, Memetic
Description: Provides the user with innate knowledge of very simple anomalous memes. If used correctly, can be really annoying. If used incorrectly, can be even more annoying.
Card Title: Dr. Mann’s Six-Shooter
Subtitle: Ole Reliable
Type: Weapon, Ranged
Description: A revolver used by infamous Foundation doctor Everett Mann. Bonus to accuracy. Bonus to damage vs. undead. Bonus to lunacy.
Stats: +10 ATK / +3 ACC
Card Title: Intro to Unethical Business Practices, 5th Edition
Subtitle: A MC&D Educational Product
Type: Ability, Memetic
Description: Anyone holding this book receives a +3 bonus to smooth-talking, negotiations, salesmanship, and swindling. Side effects include greasy combed-back hair and cheap suits.
Card Title: The Infinity Gun10
Subtitle: An Abomination
Type: Weapon, Ranged
Description: A gun made of a god bound by the shredded souls of nine innocents. Instantly annihilates one being or artifact anywhere on the board, so long as the user is able to describe it. Expires after use.
Stats: +∞ ATK / -50 Morality
An Orange Card.
Card Title: DEER
Subtitle: God From The Stars
Type: Anomaly, Unknown, Supreme Divine
Description: An ancient and mysterious creature that fell from the stars and broke the masquerade. Incredibly powerful.
Stats: 34 ATK / 28 DEF / 40 HP
Card Title: Technicolor Dream Goat
Subtitle: Goat With All The Colors Of The Wind
Type: Anomaly, Sentient, Animal
Description: A multicolor, ethereal goat. Said to fill the dreams of its victims with incessant bleating. Impervious to physical attacks.
Stats: 2 ATK / 3 DEF / 4 HP
Card Title: Mr. God
Subtitle: Has A Himself Complex
Type: Anomaly, Sentient, Divine
Description: An old man with a penchant for the dramatic. Hasn’t worn anything other than sandals for a thousand years. Needs to get off his high horse.
Stats: 15 ATK / 8 DEF / 23 HP
Card Title: A Bundle Of Golems
Subtitle: The World’s Cutest Chemicals
Type: Anomaly, Sentient, Construct
Description: Little statue people made of elements. Tend to engage in shenanigans. Legend has it they unite once a year to form a powerful Anart Mecha, but this is unconfirmed.
Stats: 5 ATK / 8 DEF / 5 HP
Card Title: A Very Angry Star
Subtitle: I Mean, Really Very Angry
Type: Anomaly, Sentient, Divine
Description: A furious ball of plasma and gas with a specific hatred for Planet Earth. The reason for its rage is uncertain, though it is known that Alto Clef owes it $23.50.
Stats: 24 ATK / 18 DEF / 20 HP
Card Title: Corrosion Man
Subtitle: The Snatcher
Type: Anomaly, Sentient, Humanoid
Description: A ghastly former soldier turned into a living nightmare. Steals children for probably horrible reasons. Lives in an attic above the Darkness Between Dimensions.
Stats: 9 ATK / 9 DEF / 9 HP
Card Title: L O N G C O R G
Subtitle: Stretch Doggo
Type: Anomaly, Sentient, Animal
Description: An immensely long corgi. Used for public transportation. A very good boy.
Stats: 3 ATK / 5 DEF / 18 HP
Card Title: Wretched Bovine Heart
Subtitle: Beating In The Darkness
Type: Anomaly, Sentient, Biological
Description: A demon possessed by speed incarnate. Horrifying and unrelenting. All entities nearby lose -2 to DEF against psychological threats.
Stats: 17 ATK / 5 DEF / 6 HP
Card Title: The Serpent
Subtitle: The Source of Knowledge
Type: Anomaly, Sentient, Supreme Divine
Description: Tricked Adam El Asem into eating from the Tree of Knowledge, freeing itself from its prison. Legends say the Wanderer’s Library is built on its back. Thinks it knows everything. Probably does.
Stats: 10 ATK / 35 DEF / 45 HP
Card Title: SCP-173 In A Sombrero
Subtitle: Ay Caramba
Type: Anomaly, Construct
Description: A statue in a sombrero. Gave up a life of snapping necks to pursue its dream of dancing. Nearby entities may be overcome by the urge to dance.
Stats: 4 ATK / 9 DEF / 8 HP
A Yellow Card.
Card Title: A Little Insurgency
Description: Your friends in the Insurgency carry out a coup on one of your rivals. To the victor goes the spoils! I’m sure they won’t take it personally.
Effect: Receive all of the earnings of a random player.
Card Title: Research Grant
Description: You are offered a grant for your contributions to academia. Congratulations! Now hurry up and spend it before the Foundation scoops you up.
Effect: Receive $300.
Card Title: Lost Aztec Gold
Description: You discover a bounty of gold from a ruined civilization! Enjoy your blood money!
Effect: Receive $300.
Card Title: Business Deal
Description: You negotiate a deal between The Factory and Dr. Wondertainment, and earn big dividends for your work!
Effect: Receive $400.
Card Title: Mr. Money’s Jackpot Extravaganza
Description: Mr. Money is about to make you Mr. Wealthy!
Effect: Receive $5000.
A White Card.
Card Title: Elixir
Description: You get ahold of the Overseer’s Secret Stuff. You are healed!
Effect: When used, completely heals the player of all ailments.
Card Title: Safe Passageway
Description: In a stroke of good luck, you discover a road untouched by danger!
Effect: Allows a player to bypass a dangerous environmental hazard unharmed.
Card Title: Promotion
Description: Your efforts do not go unnoticed! You are promoted to Senior Junior Researcher!
Effect: Player gains a permanent +2 to ATK and +3 to DEF.
Card Title: Brushed By The Starfish
Description: The Cosmic Starfish brushes against you with one of its Five arms. You are empowered!
Effect: Player receives +10 ATK / +10 DEF / +8 SPD / +10 HP for three turns.
Card Title: Angel of Mercy
Description: You are resurrected!
Effect: If a player is killed while holding this card, the player is instantly returned to half health.
A Black Card.
Card Title: Fuck This One Guy Specifically
Description: Someone has a grudge! The Sun God Nergal punches you! Ouch!
Effect: The player is punched by a Supreme Divine being, and dies.
Card Title: Containment Breach
Description: Oh no! All of your contained anomalies are loose! What a disastrous circumstance!
Effect: Any contained anomalies return to the gameboard.
Card Title: Friend or Foe
Description: Your companion betrays you, seeking glory only for themselves!
Effect: Any companions become hostile to the player.
Card Title: Consolidation11
Description: A deal is signed. The Foundation dissolves! Only the Coalition remains!
Effect: Removes the MR. CONTAINMENT and MR. FOUNDATION win conditions.
Card Title: ████ (••/•••••/••/•)
Description: Bad luck, hombre.
Effect: Player is killed.12
A Purple Card.
Card Title: Mr. Moon
Subtitle: Waxing and Waning
Type: Wondertainment
Description: The great and terrible Mr. Moon! Disrupts the tides and summons werewolves across the map! Not made of cheese! Maybe made of cheese!
Stats: 19 ATK / 20 DEF / 20 HP
Addendum 3301.4: Memorandum Regarding SCP-3301
From: Dr. Tilda Moose, Director, Site-19
To: 3301 Research Team
CC: Ethics Committee Liaison, Director Council Liaison, Classification Committee Liaison
Last night, one week since the last playthrough of SCP-3301, a small shipment of these games were discovered on a truck destined for a toy store in Wisconsin. We began a game immediately after discovering these games, and received a note through our game board thanking us for our continued playtesting. The anomalous games were promptly removed before they could be viewed by a larger audience, but this was still too close of a call.
Truthfully, we don’t know how they're getting all of this information. INFOSEC teams have advised me that these were likely warning shots, something innocuous that could be easily detected and quickly removed, but a notice that additional measures may be taken if we don’t comply.
After consulting with our security teams, and members of the Site Director Council, we’ve decided to do just that. In a break from our typical mantra, access restrictions to SCP-3301 have been reduced considerably, and the object has been reclassified as Safe. So far as we can tell, this is a legitimate show of good faith, with no malicious intent. For some reason, Dr. Wondertainment has provided us with something fun, and wants us to play it. In this case, we’ll do just that.
Our protocol for this object does not reflect a change in our policy regarding using anomalous objects for recreational purposes. Nor does it reflect a change in our relationship with the group of interest known as Dr. Wondertainment. For all intents and purposes, this new protocol is the containment procedure for this object, and this object only.
-Moose
Addendum 3301.5: SCP-3301 Testing and Gameplay Logs
In keeping with proper Foundation testing protocols, all instances of SCP-3301’s active state are to be recorded for analysis and archival. The following is an example of proper test log format, and should be used in all future instances of test logging.
SCP-3301 Test and Gameplay Log:
Log ID: 3301|001
Participants: Dr. Andrew Richards and Agent Anna Lang, Dr. Michael St. Clair and Dr. Isaac Baker, Dr. Nicholas Quinn and Dr. Django Bridge, Agent Julian Calloway and Agent Jasper Jenkins
Game Board: The Garden of Eden
Winner: Dr. Nicholas Quinn and Dr. Django Bridge
Victory Condition: MR. ESCHATOLOGY
Runtime: 3h 14m 58s
Payout: $1,750
Game Summary: Dr. Richards got out to an early start when Agent Lang managed to contain two entities within the first five turns, having drawn the card “Mr. Containment’s Vac-n-Suck” on the first turn. However, the tides began to turn when Dr. Bridge, on the direction of Dr. Quinn, opened a secret treasure chest in a cave and found the card “Sacrifice”, a red card that he then used to summon “Planet of Ten Thousand Fingers”, a supreme divine orange card.
While Dr. St. Clair and Agent Calloway’s teams were fending off attacks from the first supreme divine being, Dr. Quinn drew the red card “Error in the Database", which summoned the supreme divine being “Mary Nakayama” on the 16th turn. This being immediately dispatched Agent Calloway’s team, which was already weakened by the “Planet of Ten Thousand Fingers”.
However, on the 29th turn, Dr. Richards drew a purple card, Ms. Sweetie, which cast a sugary haze across the entire map. Agent Lang then spent the next three turns attacking Ms. Sweetie, attempting to contain the entity before Dr. Quinn and Dr. Bridge could summon another supreme divine being. Their efforts were disrupted by Dr. Baker, who sniped Agent Lang from a nearby hillside, and then began attacking the already damaged Ms. Sweetie.
On the next turn, Dr. Quinn drew the card ”Upgrade”, which he used on an item that Dr. Bridge had recovered called “Puzzle Box”. The result was a card called “Celestial Puzzle Box”, which Dr. Quinn used to summon “A Clockwork God”, the third supreme divine being, which immediately ended the game. After the usual fireworks and musical celebration, the voice of the announcer declared that Dr. Quinn and Dr. Bridge had become MR. ESCHATOLOGY.
At the conclusion of the game, Dr. Quinn and Dr. Bridge were awarded their payout, a gold bar worth exactly $1,750.
Audio Recording Transcript Excerpts
[BEGIN LOG]
Dr. Quinn: Alright, so… let’s see. I’m going to use “Clockwork Fanaticism” on Django.
Dr. St. Clair: What’s that do?
Dr. Quinn: It, uh… imbues him with clockwork powers. (Pauses) I think that’s it. (To Bridge) You noticing anything different down there?
Dr. Bridge: (Distantly) Yes, actually. I seem to have had my insides replaced with gears and pulleys. Oh! (Pauses) So now I seem to have something being created inside my stomach. Hang on. (Pauses) Yes, I’ve now deposited it into my hands.
Dr. Quinn: Cool, I get to draw a card. (Pauses to read) So that’s called a “Puzzle Box”. It doesn’t look like it has any stats. Can it do anything?
Dr. Bridge: Doesn’t, uh… doesn’t seem to do anything.
Dr. Quinn: Huh. Alright. I guess we’ll just discard that later.
[END LOG]
[BEGIN LOG]
Dr. Quinn: (Laughs) OK, so listen to this. This is called “Error in the Database”, and if we find a computer-
Dr. Bridge: Which I’m standing right next to, yes.
Dr. Quinn: -then I can summon a supreme divine-
Agent Calloway: Oh my god this is bullshit.
Agent Jenkins: What? What’s going on up there?
Dr. Quinn: -called Mary Nakayama. Bite my ass, Calloway.
Agent Jenkins: Hey Julian? Something going on up there? Because things are starting to get a little spooky down here.
Agent Calloway: Hang on, uh… I’ve got… uh…
Agent Jenkins: So are you going to—
Agent Jenkins is suddenly annihilated when a spectral entity appears where he was standing.
Agent Calloway: Oh goddammit.
[END LOG]
[BEGIN LOG]
Dr. St. Clair: Take the shot, Baker!
The sound of a distant gunshot.
Dr. St. Clair: Hell yeah!
Dr. Richards: Anna? Anna? Ah, fuck it. What is that thing, and when did you draw it? I didn’t see it when I divined you earlier.
Dr. St. Clair: In the last turn! How clutch is that?
Dr. Baker: Mike? Hey, Mike? Did we win?
Dr. St. Clair: Nah, not yet, but you get another shot into that Mister and we will.
Dr. Quinn: Is that so, Michael?
Dr. St. Clair: Oh no you don’t. Don’t you try any more of your eschatology bullshit, Nicholas.
Dr. Quinn: Regrettably, all I have is eschatology bullshit! Behold, I use “Upgrade” on this “Puzzle Box”, to create… (pauses) a Celestial Puzzle Box! Django, open that bad boy up!
Dr. Bridge: You got it. Come on out, biggun. (The sound of grinding metal is heard in the background)
Announcer’s Voice: That’s it! The game is over! Congratulations to Django Bridge and Nicholas Quinn; you have become MR. ESCHATOLOGY!
Dr. St. Clair: God fucking dammit Nick, I almost had that one.
Further testing logs are available here.