SCP-2303, originally intended to serve as the Municipal Office Complex of Ciudad Encrucijada
Item #: SCP-2303
Object Class: Euclid
Special Containment Procedures: Site-885 has been established in proximity to SCP-2303. Foundation activities in the vicinity of the anomaly are to be presented to potential observers as a long-term architectural engineering study. Due to the lack of permanent residents in the area of SCP-2303, Minimum Security Perimeter protocols are in effect.
Monitoring and documentation of SCP-2303 phenomena is to be performed via automated means whenever feasible. In cases where direct observation must be performed, standard memetic hazard safety procedures are to be instituted.
All suspected SCP-2303 phenomena are to be independently investigated and confirmed to the extent possible before being formally entered into the research log.
SCP-2303 research is assigned to Mobile Task Force Phi-9 ("Barqueros"). Members of MTF Phi-9 are currently subject to provisional security clearance authorization. Provisional status will be reevaluated on ██/██/████, per the standard five year probationary period for Special Recruitment personnel.
Description: SCP-2303 is an abandoned high-rise building, located in the ghost city of Ciudad Encrucijada1, in the Río Negro province of Argentina. SCP-2303 is the apparent focal point for recurring episodes of anomalous information transfer. As observed through specialized methodology and equipment, SCP-2303 is host to a phenomenon wherein concepts and ideas that are not fully realized and never implemented by their originators are communicated to observers in the vicinity. These concepts range widely across the spectrum of human endeavor, and have in the past included proposed artworks, philosophical schools of thought, political systems, scientific theorems, and public works projects.
Information transmitted within SCP-2303 may be observed through a variety of media. Appliances such as antenna-equipped televisions and radios which are powered on but not set to an occupied frequency are capable of receiving and displaying audio and visual information. These typically take the form of short, sporadic bursts of interrelated information, such as sequential scenes from a motion picture, or related tracks from a musical album. SCP-2303 has been outfitted with monitors and recording devices throughout its structure in order to observe this information.
In order to observe more abstract concepts, such as philosophical precepts or hypothetical political movements, direct observation is required. A methodology for discerning the outside influence of SCP-2303 (as opposed to inherent thought processes in the observer) was developed by former Group of Interest GX-5573, a loose collective of academics, artists and amateur researchers from throughout southern Argentina who first encountered the anomaly. This methodology incorporates elements of autohypnosis, meditation techniques developed by indigenous Tehuelche tribespeople, and activities similar to the Spiritualist practices of automatic writing and psychometry.
Individual works and concepts encountered within SCP-2303 are observable for a time period of between three days and five weeks. Information presented within SCP-2303 will become less contextualized and coherent from occurrence to occurrence, until the unifying concept decays and is no longer observable.
The manifestation of ideas presented in SCP-2303 do not appear to be linked chronologically to their originators. While concepts imagined within the past thirty years comprise the majority of observed data, older ideas have been documented, in some cases originating hundreds of years prior to observation.
Addendum 2303.1 - Concepts Observed Within SCP-2303
Floors 1-12: Artistic Works
Floors 13-19: Religion and Philosophy
Floors 20-27: Public Works and Large Scale Coordinated Projects
Floors 28-31: Science
Floor 32: CLASSIFIED
Addendum 2303.2 - MTF Phi-9 Introductory Statement
FROM: Rojas, Aurelio
TO: Guest4939
RE: <none>
Attachment: HANDBOOK_PHI_9.pdf, securityapp.pdf, HR_rates_plans.xls, OPEN_THIS_LAST.xyx, DONT_OPEN.aad
Let's get this thing out of the way. This is Phi-9, and most of us were inducted to the Foundation from outside. You are joining us with none of our history.
That's good.
You're a Barquero now. That means you ferry this stuff to wherever the hell it goes after it's gone. The bosses above told you to document this and research it and what have you. Don't worry, we're going to do plenty of that.
But really, you're here to watch these things in the tower go away and disappear. To make sure they go away and disappear. Eduardo calls them dreams but he's an asshole and not to be trusted. You'll find that out soon.
All the formal stuff and the manuals are there. You'll read them eight times without me needing to tell you. Oh man, the culture shock. Most of you aren't from the Southern Hemisphere. We do things different. Yeah, read the manuals. Then close those and listen to me.
You're going to be in that tower soon. Exciting, right? They tell me people ask to be here. You're going to read the manuals no matter what I tell you, so you're going to be on your guard, so much that you think you'll never let it down again.
Bullshit. One day, and it's going to be soon, I promise you, you're going to think "hey, that one's a pretty good idea, bring that one out, man." I know you are. It happens a lot. It happened to me on the top floor. I thought I knew better than this madhouse that we all were so smitten by. I told everyone that we needed to save something from here. We should pull it back out I said, this is just too beautiful. And it was. We all dropped to our knees to see it, even after it started doing what it did.
When it was over, I ran as fast as I could into the night to find anyone that could help us put things back. There was a lot to clean up.
Here's your first order, don't ask about the other Barqueros, from before this was an official outfit. There's eight of us left from the top floor thing. There were a lot more before.
Here's your other first order. You leave that shit in the tower. Every single thing you see in there is a painted corpse. That's because it's a grave. All of it stays in there to rot and die, no matter what, or I put the bullet in your head myself. I look out for my people, and you're one of mine now.
Welcome aboard, Barquero.
ROJAS